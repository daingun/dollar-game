/** Represent a Graph. */
class Graph {
    /**
     * @param {number} vertices - The number of vertices of the graph.
     */
    constructor(vertices) {
        this.vertices = vertices;
        this.edges = 0;
        this.graph = createGraph(vertices);
    }
    
    /**
     * Add an edge to the graph.
     * @param {number} v1 - First vertex.
     * @param {number} v2 - Second vertex.
     */
    addEdge(v1, v2) {
        this.graph.get(v1).appendEdge(v2);
        this.graph.get(v2).appendEdge(v1);
        this.edges++;
    }

    /**
     * Propagate 1 unity for each connected vertex.
     * @param {number} vertex - Vertex that give its value.
     */
    propagate(vertex) {
        this.graph.get(vertex).give();
        this.graph.get(vertex).edges.forEach(
            v => this.graph.get(v).increaseValue()
        );
    }

    /**
     * Check victory condition. All value shall be non negative.
     * @return {boolean} True when all values are non negative.
     */
    checkVictory() {
        const list = Array.from(this.graph, ([k, v]) => v.value >= 0);
        return list.reduce((acc, curr) => acc && curr, true);
    }

    /**
     * Set random edges between the vertices.
     */
    setEdges() {
        const edges_number = getRandomInt(this.vertices, 2 * this.vertices);
        for (let i = 0; i < edges_number; ++i) {
            const v1 = getRandomVertex(this.vertices);
            const v2 = getRandomVertex(this.vertices);
            if (v1 === v2) {
                continue;
            }
            this.addEdge(v1, v2);
        }
    }

    /**
     * Randomly push values around.
     * @param {number} shuffles - number of times values are moved.
     */
    shuffleValues(shuffles) {
        for (let i = 0; i < shuffles; ++i) {
            this.propagate(getRandomVertex(this.vertices));
        }
    }

    /**
     * Add values to the vertices depending on the difficulty
     * of the game.
     * @param {string} difficulty - "hard"==empty, "medium", "easy"
     */
    setValues(difficulty) {
        let g = genus(this.edges, this.vertices);
        if (difficulty === "easy") {
            g += 5;
        } else if (difficulty === "medium") {
            g += 2;
        }
        for (let i = 0; i < g; ++i) {
            const v = getRandomVertex(this.vertices);
            this.graph.get(v).increaseValue();
        }
    }

    /**
     * String representation of a Graph.
     * @return {string} Graph representation.
     */
    toString() {
        return Array.from(this.graph,
            ([k, v]) => "Vertex " + k + " - " + v.toString()
        );
    }

    /**
     * print a Graph on the console.
     */
    print() {
        this.toString().forEach(s => console.log(s));
    }
}

/**
 * Create a graph.
 * @param {number} vertices - The number of vertices of the graph.
 * @return {Map} Initialized graph.
 */
function createGraph(vertices) {
    const graph = new Map();
    for (let i = 1; i <= vertices; i++) {
        graph.set(i, new Vertex(0));
    }
    return graph;
}

/** Represent a vertex. */
class Vertex {
    /**
     * Create a vertex.
     * @param {number} value - The value of the vertex.
     */
    constructor(value) {
        this.value = value;
        this.edges = new Set();
    }

    /**
     * Add an edge from the current vertex.
     * Use addEdge defined on Graph to add edges.
     * @param {number} v - Other vertex.
     */
    appendEdge(v) {
        this.edges.add(v);
    }

    /**
     * Increase by one the value of the current vertex.
     */
    increaseValue() {
        this.value += 1;
    }

    /**
     * Decrease the value of the current vertex.
     * @param {number} n - The amount to decrease the current vertex value.
     */
    decreaseValue(n) {
        this.value -= n;
    }

    /**
     * Decrease the value giving 1 to all connected vertices.
     */
    give() {
        this.decreaseValue(this.edges.size);
    }

    /**
     * String representation of a Vertex.
     * @return {string} Vertex representation.
     */
    toString() {
        return this.value;
    }
}

/**
 * Return the maximum number of possible edges of a graph.
 * @param {number} v - Number of vertices.
 * @return {number} The maximum number of edges.
 */
function maxEdges(v) {
    return (v - 1) * v / 2;
}

/**
 * Check that the number of edges is less that the maximum.
 * @param {number} edges - Number of chosen edges.
 * @param {number} vertices - Number of vertices of the graph.
 * @return {boolean} True if check is passed.
 */
function checkMaxEdges(edges, vertices) {
    return edges <= maxEdges(vertices);
}

/**
 * Return the genus of the graph.
 * @param {number} edges - The number of edges of the graph.
 * @param {number} vertices - The number of vertices of the graph.
 * @return {number} Genus of the graph
 */
function genus(edges, vertices) {
    // Edges - vertices + faces.
    return edges - vertices + 1;
}

/**
 * Get a random integer between start and stop included.
 * @param {number} start - Lower bound included.
 * @param {number} stop - Higher bound included.
 * @return {number} Random integer.
 */
function getRandomInt(start, stop) {
    // [start..stop]
    return Math.floor(Math.random() * Math.floor(stop) + start);
}

/**
 * Get a random vertex.
 * @param {number} vertices - The number of vertices of the graph.
 * @return {number} Random vertex.
 */
function getRandomVertex(vertices) {
    // [1..vertices]
    return getRandomInt(1, vertices);
}

/**
 * Create the elements of the UI.
 * @param {number} graph - The Graph to build the UI.
 */
function gameSetup(graph) {
    const pol = new Polygon(graph.vertices, 300, 300, 200);
    pol.draw();

    graph.graph.forEach(
        function(v, k) {
            const vert = document.getElementById("vertex-" + k);
            vert.onclick = function(event) {
                const id = event.target.id.split("-")[1];
                graph.propagate(Number.parseInt(id, 10));
                update(graph);
            }
            const node = document.getElementById("node-" + k);
            node.textContent = v.toString();

            pol.addLines(k, v.edges);
        }
    );
}

/*
 * Update the UI afer graph changes.
 * @param {number} graph - the Graph for the update.
 */
function update(graph) {
    graph.print();
    graph.graph.forEach(
        function(v, k) {
            const node = document.getElementById("node-" + k);
            node.textContent = v.toString();
        }
    )
    checkResult(graph);
}

/*
 * Check the result and print the result.
 */
function checkResult(graph) {
    const result = document.getElementById("result");
    if (graph.checkVictory()) {
        result.innerText = "Victory!";
    } else {
        result.innerText = "All vertex shall be non negative";
    }
}

/*
 * Clears the svg element.
 */
function clearBoard() {
    document.getElementById("result").innerText = "Click on a vertex to distribute money";
    const svg = document.getElementById("gameSvg");
    while (svg.firstChild) {
        svg.removeChild(svg.firstChild);
    }
}

class Polygon {
    /*
     * Construct a Polygon given the number of vertices and the coordinates
     * of its centre.
     * @param {number} vertices - the number of vertices of the polygon.
     * @param {number} centreX - the X coordinate of the centre.
     * @param {number} centreY - the Y coordinate of the centre.
     * @param {number} radius - the radius of the vertices circle.
     */
    constructor(vertices, centreX, centreY, radius) {
        this.vertices = vertices;
        this.centreX = centreX;
        this.centreY = centreY;
        this.radius = radius;
        this.vertex_radius = 40;
    }

    /*
     * Calculate the versor of each polygon vertex.
     * @param {number} vertex - vertex to calculate the versor.
     * @return {number, number} versor.
     */
    getVertexVersor(vertex) {
        const angle = (vertex - 1) / this.vertices * 2 * Math.PI;
        const x = Math.cos(angle);
        const y = Math.sin(angle);
        return {x: x, y: y};
    }

    /*
     * Calculate the position of the vertex.
     * @param {number} vertex - vertex number.
     * @return {number, number} position.
     */
    getVertexPosition(vertex) {
        const unit = this.getVertexVersor(vertex);
        const x_pos = this.centreX + this.radius * unit.x;
        const y_pos = this.centreY + this.radius * unit.y;
        return {x: x_pos, y: y_pos};
    }

    /*
     * Calculate the position of the line end.
     * @param {number} vertex - vertex number.
     * @return {number, number} position.
     */
    getLabelPosition(vertex) {
        const unit = this.getVertexVersor(vertex);
        const distance = this.radius + this.vertex_radius * 1.5;
        const x_pos = this.centreX + distance * unit.x;
        const y_pos = this.centreY + distance * unit.y;
        return {x: x_pos, y: y_pos};
    }

    /*
     * Calculate the position of the line end.
     * @param {number} vertex - vertex number.
     * @return {number, number} position.
     */
    getLineEndPosition(vertex) {
        const unit = this.getVertexVersor(vertex);
        const distance = this.radius - this.vertex_radius;
        const x_pos = this.centreX + distance * unit.x;
        const y_pos = this.centreY + distance * unit.y;
        return {x: x_pos, y: y_pos};
    }

    /*
     * Draw the polygon in the svg.
     */
    draw() {
        const svg = document.getElementById("gameSvg");
        for (let i = 1; i <= this.vertices; ++i) {
            const point = this.getVertexPosition(i);
            svg.appendChild(this.createNode(i, point));
            svg.appendChild(this.createVertex(i, point));
            const label_position = this.getLabelPosition(i);
            svg.appendChild(this.createLabel(i, label_position));
        }
    }

    /*
     * Create the vertex.
     * @param {number} i - index of the vertex.
     * @param {point} position - position of the vertex.
     * @return {circle} SVG circle.
     */
    createVertex(i, position) {
        const circle = document.createElementNS(SVG_NS, "circle");
        circle.id = "vertex-" + i;
        circle.setAttributeNS(null, "class", "vertex");
        circle.setAttributeNS(null, "cx", position.x);
        circle.setAttributeNS(null, "cy", position.y);
        circle.setAttributeNS(null, "r", this.vertex_radius);
        circle.setAttributeNS(null, "stroke", "black");
        circle.setAttributeNS(null, "fill", "white");
        circle.setAttributeNS(null, "fill-opacity", 0); // Transparent.
        return circle;
    }

    /*
     * Create the node.
     * @param {number} i - index of the node.
     * @param {point} position - position of the node.
     * @return {text} SVG text.
     */
    createNode(i, position) {
        const node = document.createElementNS(SVG_NS, "text");
        node.id = "node-" + i;
        node.setAttributeNS(null, "class", "node");
        node.textContent = i;
        node.setAttributeNS(null, "x", position.x);
        node.setAttributeNS(null, "y", position.y);
        return node;
    }

    /*
     * Create the label.
     * @param {number} i - index of the label.
     * @param {point} position - position of the vertex of the label.
     * @return {text} SVG text.
     */
    createLabel(i, position) {
        const label = document.createElementNS(SVG_NS, "text");
        label.id = "label-" + i;
        label.setAttributeNS(null, "class", "label");
        label.textContent = i;
        label.setAttributeNS(null, "x", position.x);
        label.setAttributeNS(null, "y", position.y);
        return label;
    }

    /*
     * Create the lines between the vertices.
     * @param {number} i - vertex index.
     * @param {Set} edges - list of vertices connected to the current vertex.
     */
    addLines(i, edges) {
        const svg = document.getElementById("gameSvg");

        const currentNode = this.getLineEndPosition(i);
        for (let o of edges){
            const otherNode = this.getLineEndPosition(o);
            const line = document.createElementNS(SVG_NS, "line");
            line.setAttributeNS(null, "x1", currentNode.x);
            line.setAttributeNS(null, "y1", currentNode.y);
            line.setAttributeNS(null, "x2", otherNode.x);
            line.setAttributeNS(null, "y2", otherNode.y);
            line.setAttributeNS(null, "stroke", "black");
            svg.appendChild(line);
        }
    }
}

//let vertices = [1, 2, 3, 4, 5, 6];
//let maximum_edges = vertices.map(v => max_edges(v));
//console.log(maximum_edges);

console.log(checkMaxEdges(7, 6));
console.log(checkMaxEdges(7, 3));

const SVG_NS = "http://www.w3.org/2000/svg";

startButton.addEventListener("click", function() {
    clearBoard();
    const startButton = document.getElementById("startButton");
    const difficulty = document.getElementById("difficulty");
    const nodes = document.getElementById("nodes");

    const diff = difficulty.selectedOptions[0].value;
    const n = parseInt(nodes.value, 10);

    let graph = new Graph(n);
    console.log(graph);

    graph.setEdges();
    graph.setValues(diff);
    graph.shuffleValues(10);
    graph.print();
    console.log(graph.checkVictory());

    gameSetup(graph);
});
